from __future__ import print_function

import os.path

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

# If modifying these scopes, delete the file token.json.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']

# The ID and range of a sample spreadsheet.
SPREADSHEET_ID = '1MLSE9VCtMcnlQqyQT4w4VEYnBQ4o9UxT6favRZoxWP0'
VOLUNTEERS_RANGE_NAME = 'A23:O100'
SHIFTS_RANGE_NAME = 'E3:I11'
MAIL_RANGE_NAME = ''

def login():
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    creds = None
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.json'):
        creds = Credentials.from_authorized_user_file('token.json', SCOPES)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.json', 'w') as token:
            token.write(creds.to_json())
        return creds

def new_volunteer(values):
    volunteers = []
    for row in values:
        if (row[0] == '2'):
            print("Nouveau bénévole à transmettre à Thierry : ", row[3], row[4])
            volunteers.append([row[1], row[3], row[4], row[6:17]])
            # UPDATE STATUS
    send_mail_validation(volunteers)
    print("Fin du traitement des nouveaux bénévoles")

def send_mail_validation(volunteers):
    f = open("mail_thierry", 'w+')
    f.write("Bonjour Thierry,\nVoici mes bénéoles du jour :\n")
    for row in volunteers:
        print(row)
        f.write(row[0] + ' ' + row[1] + " " + row[2] + ' ')
        if (row[3][0] != '0'):
            f.write(row[3][0] + ' 15/04 SOIREE;')
        if (row[3][1] != '0'):
            f.write(row[3][1] + ' 16/04 JOURNEE;')
        if (row[3][2] != '0'):
            f.write(row[3][2] + ' 16/04 JOURNEE;')
        if (row[3][3] != '0'):
            f.write(row[3][3] + ' 16/04 JOURNEE;')
        if (row[3][4] != '0'):
            f.write(row[3][4] + ' 16/04 SOIREE;')
        if (row[3][5] != '0'):
            f.write(row[3][5] + ' 17/04 JOURNEE;')
        if (row[3][6] != '0'):
            f.write(row[3][6] + ' 17/04 JOURNEE;')
        if (row[3][7] != '0'):
            f.write(row[3][7] + ' 17/04 JOURNEE;')
        if (row[3][8] != '0'):
            f.write(row[3][8] + ' 17/04 SOIREE;')
        f.write("\n")
    f.write("Merci et à bientôt")
    f.close()


def get_volunteer_info(shift_data, mail = False, covid = False):
    f = open('data_main', 'w+')
    n = 0
    for row in shift_data:
        print(row)
        if (n == 1):
            f.write("SAMEDI\n")
        elif (n == 5):
            f.write("DIMANCHE\n")
        if (n == 0):
            f.write("VENDREDI\n")
            if (row[0] != '0'):
                f.write('19h - 01h VESTIAIRE    : ' + row[0] + ' places\n')
            if (row[2] != '0'):
                f.write('19h - 01h ENVIRONEMENT : ' + row[2] + ' places\n')
            if (row[4] != '0' and covid):
                f.write('19h - 01h COVID        : ' + row[4] + ' places\n')
        elif ((n % 3) == 1):
            if (row[4] != '0'):
                f.write('06h - 10h ENVIRONEMENT : ' + row[2] + ' places\n')
        elif ((n % 4) == 2):
            if (row[0] != '0'):
                f.write('09h - 14h ENVIRONEMENT : ' + row[0] + ' places\n')
            if (row[2] != '0'):
                f.write('09h - 14h VESTIAIRE    : ' + row[4] + ' places\n')
            if (row[4] != '0' and covid):
                f.write('09h - 14h COVID        : ' + row[4] + ' places\n')
        elif ((n % 4) == 3):
            if (row[0] != '0'):
                f.write('14h - 19h ENVIRONEMENT : ' + row[0] + ' places\n')
            if (row[2] != '0'):
                f.write('14h - 19h VESTIAIRE    : ' + row[2] + ' places\n')
            if (row[4] != '0' and covid):
                f.write('14h - 19h COVID        : ' + row[4] + ' places\n')
        elif ((n % 4) == 0):
            if (row[0] != '0'):
                f.write('19h - 01h VESTIAIRE    : ' + row[0] + ' places\n')
            if (row[2] != '0'):
                f.write('19h - 01h ENVIRONEMENT : ' + row[2] + ' places\n')
            if (row[4] != '0' and covid):
                f.write('19h - 01h COVID        : ' + row[4] + ' places\n')
        n = n + 1
    f.close()
    if (mail):
        send_mail_volunteer()
    
def send_mail_volunteer():
    print("PLACEHOLDER")
            
def main():

    print("Authentification en cours")
    creds = login()
    print("Authentification réussie")
    print("Récupération des données des bénévoles")
    try:
        service = build('sheets', 'v4', credentials=creds)

        # Call the Sheets API
        sheet = service.spreadsheets()
        result = sheet.values().get(spreadsheetId=SPREADSHEET_ID,
                                    range=VOLUNTEERS_RANGE_NAME).execute()
        values = result.get('values', [])

        if not values:
            print('Aucune donnée trouvé')
        else:
            print("Données récupérées avec succès")
            new_volunteer(values)

        print('Récupérations des données des besoins')
        result = sheet.values().get(spreadsheetId=SPREADSHEET_ID,
                                    range=SHIFTS_RANGE_NAME).execute()
        shift_data = result.get('values', [])

        if not shift_data:
            print('Aucune donnée trouvé')
        else:
            print("Données de shifts récuprée")
            get_volunteer_info(shift_data)
            
        
    except HttpError as err:
        print(err)


if __name__ == '__main__':
    main()
